var express = require('express');
var app = express();
var _ = require('underscore');
var db = require('./db.js');

var PORT = process.env.PORT || 3000;
var todo = [];
var todoNextId = 1;

var bodyParser = require('body-parser');

app.use(bodyParser.json());

app.get('/',function(req,res){
    res.send('To Do Api Root.');
});

//GET /todos
app.get('/todos',function(req,res){
    var queryParams = req.query;
    var where ={};
    
    if(queryParams.hasOwnProperty('completed') && queryParams.completed === 'true'){
        where.completed = true;
    }else if(queryParams.hasOwnProperty('completed') && queryParams.completed === 'false'){
        where.completed = false;
    }
    
     if(queryParams.hasOwnProperty('q') && queryParams.q.length > 0){
         
         where.description = {
             $like:'%' +queryParams.q + '%'
         };
     }
    
    db.todo.findAll({where:where}).then(function(todos){
        res.json(todos);
    },function(e){
        res.status(500).send();
    });
    
//    var filterToDos = todo;
//    
//    if(queryParams.hasOwnProperty('completed') && queryParams.completed === 'true'){
//        filterToDos = _.where(filterToDos,{completed:true});
//    }else if(queryParams.hasOwnProperty('completed') && queryParams.completed === 'false'){
//         filterToDos = _.where(filterToDos,{completed:false});
//    }
//    
//    if(queryParams.hasOwnProperty('q') && queryParams.q.length > 0){
//        filterToDos = _.filter(filterToDos, function(nod){
//            return nod.description.toLowerCase().indexOf(queryParams.q.toLowerCase()) > -1;
//        })
//    }
//    
//   res.json(filterToDos); 
});

app.get('/todos/:id',function(req,res){
    
    var todoId = parseInt(req.params.id,2);
    
    db.todo.findById(todoId).then(function(td){
        if(!!td){ // (!!) two exclamation checking whether there are something in object or null.
            res.json(td.toJSON());
        }else{
            res.status(404).send();
        }
    },function(err){
        res.status(500).send();    
    });
//    var matchToDo = _.findWhere(todo, {id:todoId});
//    console.log(matchToDo);                  
    
//    var todo_obj;    
//    if(typeof todoId === 'undefined'){
//        
//    }else{
//        todo.forEach(function(item){
//            if(item.id === todoId){
//                todo_obj = item;     
//                return;
//            }
//        });
//    }
    
//    if(typeof todo_obj != 'undefined'){
//        res.json(todo_obj);
//    }
   
//    if(matchToDo){
//         res.json(matchToDo);
//    }else{
//         res.status(404).send();
//    }
    
    //res.status(404).send();
    //res.send('Asking for todos with id of ' + req.params.id); 
});

app.post('/todos',function(req,res){
   
    var body = _.pick(req.body,'description','completed');
    //var body = req.body;
    
     db.todo.create(body).then(function(todo){
        res.json(todo.toJSON());
    },function(e){
        res.status(400).json(e);     
    });
    
//    if(!_.isBoolean(body.completed) || !_.isString(body.description) || body.description.trim().length === 0){
//        return res.status(404).send(); 
//    }
//    
//   body.description = body.description.trim();
//   body.id = todoNextId++; 
//       
//    todo.push(body);        
//    
//    res.json(todo);
//   
//    console.log('added..');
});

app.delete('/todos/:id', function(req,res){
    var todoId = parseInt(req.params.id,10);
    var deleteToDo = _.findWhere(todo, {id:todoId});
    
    if(!deleteToDo){
        res.status(404).json({"error" : "no todo item found."})
    }else{
        todo = _.without(todo,deleteToDo);  
        res.json(todo);  
    }                  
});

app.put('/todos/:id',function(req,res){
    var todoId = parseInt(req.params.id,10);
    var matchToDo = _.findWhere(todo, {id:todoId});
    var body = _.pick(req.body,'description','completed');
    var validAttribute = {};
    
    if(!matchToDo){
        return res.status(400).send();
    }
    
    if(body.hasOwnProperty('completed') && _.isBoolean(body.completed)){
        validAttribute.completed = body.completed;
    }else if(body.hasOwnProperty('completed')){
        return res.status(400).send();
    }
    
    if(body.hasOwnProperty('description') && _.isString(body.description) &&
      body.description.trim().length > 0){
        validAttribute.description = body.description;
    }else if(body.hasOwnProperty('description')){
        return res.status(400).send();
    }
    
    _.extend(matchToDo,validAttribute);
    
    res.json(matchToDo);
    
});

db.sequelize.sync().then(function(){
    app.listen(PORT,function(){
       console.log('Listening to the port ' + PORT); 
    });
});

