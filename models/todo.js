module.exports = function(sequelize, DataTypes){
    return sequelize.define('todo',{        //tablename
    description : {             //column name
        type : DataTypes.STRING,
        allowNull : false,
        validate: {
            len:[1,250]
        }
    },
    completed :{                // column name
        type : DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue : false
    }
});
};