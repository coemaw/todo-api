var Sequelize = require('sequelize');
var sequelize = new Sequelize(undefined,undefined,undefined,{
    'dialect' : 'sqlite',               //Database Type
    'storage' :  __dirname + '/basic-sql-database.sqlite' // SQLite Name
});


var Todo = sequelize.define('todo',{        //tablename
    description : {             //column name
        type : Sequelize.STRING,
        allowNull : false,
        validate: {
            len:[1,250]
        }
    },
    completed :{                // column name
        type : Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue : false
    }
});

sequelize.sync({
    //force:true
}).then(function(){ //force : true --> drop all tables and recreate it.
        
    console.log('Everything is synced.');
    
    Todo.findById(21).then(function(todo){
        if(todo){
            console.log(todo.toJSON());
        }else{
            console.log('todo Not Found');
        }
    }).catch(function(e){
        console.log(e.toJSON());
    });
    
//    Todo.create({
//       description:'goood to go.'       
//    }).then(function(todo){
//        return Todo.create({
//           description: 'nested acronym' 
//        });
//    }).then(function(){
//        //return Todo.findById(1);
//        return Todo.findAll({
//           where : {
//               description:{
//                   $like:'%go%'
//               }
//           } 
//        });
//    }).then(function(todoObjs){
//        if(todoObjs){
//            todoObjs.forEach(function(todo_){
//                console.log(todo_.toJSON());
//            });
//        }else{
//            console.log('no to do found');
//        }
//    }).catch(function(err){
//        console.log(err);
//    });
});
